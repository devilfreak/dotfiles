call plug#begin()

Plug 'dracula/vim'
Plug 'ryanoasis/vim-devicons'
Plug 'preservim/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'suan/vim-instant-markdown', {'rtp': 'after'}
Plug 'ap/vim-css-color'
Plug 'vim-python/python-syntax'
Plug 'morhetz/gruvbox'
Plug 'lervag/vimtex'

call plug#end()
