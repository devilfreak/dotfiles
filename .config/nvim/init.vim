set path+=**
set number
set autoindent
set wildmode
set hidden
set clipboard=unnamedplus
set nobackup
set noswapfile
set expandtab
set smarttab
set tabstop=4
set shiftwidth=4 
set number relativenumber
set scrolloff=7 

source $HOME/.config/nvim/vim-plug/plugins.vim

colorscheme dracula
let g:lightline = {'colorscheme': 'dracula',}
hi Normal guibg=NONE ctermbg=NONE

set mouse=nicr
set mouse=a

let g:vimtex_view_method = 'zathura'

" Remap splits navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
