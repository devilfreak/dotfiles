pcall(require, "luarocks.loader")

-- Standard awesome library
require("awful.autofocus")

-- Theme handling library
local beautiful = require("beautiful")

-- Themes :

-- choose your theme here
local accents = {
    "tomorrow-dark",   		-- 1
    "otis-forest",			-- 2
    "winter",				-- 3

}
-- choose your theme here
local chosen_accents = accents[2]
local theme_path = string.format("%s/.config/awesome/themes/accents/%s.lua", os.getenv("HOME"), chosen_accents)
beautiful.init(theme_path)

-- Autostart
awful.spawn.with_shell("~/.config/awesome/autorun.sh")

-- Configs :

-- Notifications :
require("config.notifications")

-- keybinds :
require("config.keys")

-- Menu :
require("config.menu")

-- Rules :
require("config.rules")

-- Layouts :
require("config.layouts")

-- Titlebars :
require("config.titlebar")

-- Bar :
require("config.bar")

-- Collecting Garbage :
collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)




