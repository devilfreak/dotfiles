-- ## Bar ##
-- ~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local watch = require('awful.widget.watch')
local wibox = require("wibox")
local lain = require("lain")
local helpers = require("config.helpers")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
--

-- Tags 
-- awful.util.tagnames =  { "1", "2" , "3", "4", "5", "6", "7", "8", "9" }
-- awful.util.tagnames =  { "", " ", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "dev",  www ", " sys ", "doc", "vbox", "chat", "mus", "vid", "gfx" }
-- awful.util.tagnames =  { "", "", " ", "","", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "I", "II", "III", "IV", "V", "VI" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "一", "二", "三", "四", "五", "六", "七", "八", "九" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
awful.util.tagnames =  { "", "", "", "", "", "", "", "", ""}
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", ""}

-- Pacman Taglist :
-- awful.util.tagnames = {"●", "●", "●", "●", "●", "●", "●", "●", "●", "●"}
-- awful.util.tagnames = {"", "", "", "", "", "", "", "", "", ""}
-- awful.util.tagnames = {"•", "•", "•", "•", "•", "•", "•", "•", "•", "•"}
-- awful.util.tagnames = { "","", "", "", "", "", "", "", "", "" }
-- awful.util.tagnames = { "󰮯", "󰮯", "󰮯", "󰮯", "󰮯", "󰮯", "󰮯", "󰮯", "󰮯", "󰮯" }
-- awful.util.tagnames =  { "", "", "", "", "", "", "", "", "",  "" }



-- Widgets :
-- function to add paddings and background to widgets.
local function barcontainer(widget)
    local container = wibox.widget
      {
        widget,
        top = dpi(1),
        bottom = dpi(1),
        left = dpi(4),
        right = dpi(4),
        widget = wibox.container.margin
    }
    local box = wibox.widget{
        {
            container,
            top = dpi(0),
            bottom = dpi(0),
            left = dpi(4),
            right = dpi(4),
            widget = wibox.container.margin
        },
        bg = colors.black,
        shape = helpers.rrect(dpi(6)),
        widget = wibox.container.background
    }
return wibox.widget{
        box,
        top = dpi(4),
        bottom = dpi(4),
        right = dpi(2),
        left = dpi(2),
        widget = wibox.container.margin
    }
end

-- widgets update interval (in seconds)
local interval = 60

watch('sh -c', interval, function()
    sb_battery() -- update battery status
-- volume/brightness updates everytime you incerease decrease volume through widget/keyboard shortcuts. so no need to update at intervals.
--    sb_volume()
--    sb_brightness()
end)


-- Separators :
separt = wibox.widget.textbox(" ")

-- Net :
local net = lain.widget.net{
    settings = function()
        widget:set_markup("  " .. net_now.received .. " ↓↑ " .. net_now.sent .. " ")
    end
}
net.widget.font = theme.font
mynet = wibox.widget {
	{
        {
          widget = net.widget
        },
        fg = colors.yellow, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.yellow,
	widget = wibox.container.margin
}

-- Coretemp :
local temp = lain.widget.temp{
    settings = function()
        widget:set_markup(" " .. coretemp_now .. "°C ")
    end
}
temp.widget.font = theme.font
mytemp = wibox.widget {
	{
        {
          widget = temp.widget
        },
        fg = colors.brightred, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.brightred,
	widget = wibox.container.margin
}

-- CPU :
local cpu = lain.widget.cpu{
	settings = function()
		widget:set_markup(" ".. cpu_now.usage.. "% ")
	end
}
cpu.widget.font = theme.font
mycpu = wibox.widget {
	{
        {
          widget = cpu.widget
        },
        fg = colors.blue, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.blue,
	widget = wibox.container.margin
}

-- RAM :
local mem = lain.widget.mem{
    settings = function()
        widget:set_markup(" " .. mem_now.perc .. "% ")
    end
}
mem.widget.font = theme.font
mymem = wibox.widget {
	{
        {
          widget = mem.widget
        },
        fg = colors.green, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.green,
	widget = wibox.container.margin
}

-- Battery :
local bat = lain.widget.bat{
    settings = function()
        if bat_now.status and bat_now.status ~= "N/A" then
            if bat_now.ac_status == 1 then
                --widget:set_markup(bat_now.perc, "  AC ")
                widget:set_markup(" " .. bat_now.perc .. "%")
                return
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 5 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 20 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 30 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 40 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 50 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 60 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 70 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 80 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 90 then
                widget:set_markup(" ")
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 100 then
                widget:set_markup(" ")               
            else
                widget:set_markup(" ")
            end
            widget:set_markup("  " .. bat_now.perc .. "% ")
        else
            widget:set_markup()
        end
    end
}
bat.widget.font = theme.iconfont
mybat = wibox.widget {
	{
        {
          widget = bat.widget,
        },
        fg = colors.blue, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.blue,
	widget = wibox.container.margin
}

-- Keyboard :
keyboardlayout = awful.widget.keyboardlayout()
keyboardlayout.widget.font = theme.font
mykeyboardlayout = wibox.widget {
	{
		{
			widget = keyboardlayout,
		},
        fg = colors.brightblue, -- text color
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.brightblue,
	widget = wibox.container.margin
    
}

-- Textclock :
local clock = awful.widget.watch(
    "date +'%a %d %b | %H:%M ' ", 60,
    function(widget, stdout)
        widget:set_markup("⧗ " .. stdout)
    end
)
clock.font = theme.font
myclock = wibox.widget {
	{
		{
			widget = clock
        },
        fg = colors.brightmagenta, -- text color
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.brightmagenta,
	widget = wibox.container.margin  
}

-- Calendar :
theme.cal = lain.widget.cal{
    attach_to = { clock },
    notification_preset = {
        font = theme.font,
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
}

-- Systemtry :
theme.systray_icon_spacing = dpi(6)
mytry = wibox.widget {
	{
        {
          widget = wibox.widget.systray()
        },
        fg = colors.brightgreen, -- text color
        --  bg = "blue",
        widget = wibox.container.background
	},
	--bottom = 2,
	color = colors.green,
	widget = wibox.container.margin
}

awful.screen.connect_for_each_screen(function(s)
    -- Each screen has its own tag table.
    awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
   
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox {
        screen  = s,
        buttons = {
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
            awful.button({ }, 4, function () awful.layout.inc(-1) end),
            awful.button({ }, 5, function () awful.layout.inc( 1) end),
        }
    }

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t)
				if client.focus then
					client.focus:move_to_tag(t)
				end
			end),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ modkey }, 3, function(t)
				if client.focus then
					client.focus:toggle_tag(t)
				end
			end),
        },
    }
	
    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx( 1) end),
        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar {
        position = "top",
        screen   = s,
        height = 28,
        --ontop = true,
        type = "dock",
        widget   = {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                --mylauncher,
                --s.mytaglist,
                --s.mypromptbox,
                
                -- # barcontainer :
                barcontainer(mylauncher),
                barcontainer(s.mytaglist),
                --barcontainer(s.mypromptbox),
            },
            
            -- Middle widget :
            --s.mytasklist, 
            barcontainer(s.mytasklist),
            --nil,
            
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                -- # Net :
				--mynet,
				--separt,
				barcontainer(mynet),
				
                -- # CPU TEMP :
                --mytemp,
                --separt,
                barcontainer(mytemp),
                
                -- # CPU :
                --mycpu,
                --separt,
                barcontainer(mycpu),
                
                -- # RAM :
                --mymem,
                --separt,
				barcontainer(mymem),
                
                -- # Battery :
                --mybat,
				--separt,
				barcontainer(mybat),
                
                -- # Keybord :
                --mykeyboardlayout,
                --separt,
                --barcontainer(mykeyboardlayout),
                
                -- # Clock :
                --myclock,
                --separt,
                barcontainer(myclock),
                
                -- # Sys Try :
                --mytry,
                --separt,
                barcontainer(mytry),
                
                -- # Layouts :
                --s.mylayoutbox,
                barcontainer(s.mylayoutbox),
            },
        }
    }
end)
