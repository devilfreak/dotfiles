pfetch

export EDITOR="nvim"

if status is-interactive
    alias dotfiles="/usr/bin/env git --git-dir=$HOME/git/dotfiles/.git --work-tree=$HOME/git/dotfiles"
    alias da="dotfiles add"
	alias dc="dotfiles commit -m"
	alias dp="dotfiles push"
end

set SUDO_EDITOR /usr/bin/nvim
export SUDO_EDITOR

function ls
    command exa --all --long --group-directories-first --icons $argv
end

function k
    command kubecolor $argv
end

function sue
    command sudoedit $argv
end
