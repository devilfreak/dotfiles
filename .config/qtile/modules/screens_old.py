from libqtile import bar
from .widgets import *
from libqtile.config import Screen
import os

bar_height = 23
bar_margin = [5,10,0,10]

padding_sides = 6
pic = '~/.config/qtile/eos-c.png'


#####################
#       COLOR       #
#####################


colors = [["#1D671F", "#1D671F"], # -0- background stage 1
          ["#387A3A", "#387A3A"], # -1- background stage 2
          ["#1EA023", "#1EA023"], # -2- background stage 3
          ["#4BCB50", "#4BCB50"], # -3- background stage 4
          ["#93E496", "#93E496"], # -4- middle color
          ["#91968A", "#91968A"], # -5- font color inactive
          ["#D8D8D2", "#D8D8D2"], # -6- font color
          ["#1ea023", "#1ea023"], # -7- backbround for inactive screens
          ["#0CEB22", "#0CEB22"], # -8- backbround for current screens
          ["#0B4A4C", "#0B4A4C"], # -9- underline current screen
          ["#293914", "#293914"], # -10- active font
          ["#3E3E3C", "#3E3E3C"], # -11- font color middle
          ["#4BCB50", "#4BCB50"]] # -12- background left





#####################
#      WIDGETS      #
#####################

widget_sep1 = widget.Sep(padding=padding_sides, linewidth=0, background=colors[12])
widget_pic = widget.Image(filename=pic, margin=3, background=colors[12], mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("rofi -show combi")})
widget_sep2 = widget.Sep(padding=4, linewidth=0, background=colors[12])
widget_groups = widget.GroupBox(highlight_method='line', highlight_color=colors[8], this_screen_border=colors[9], this_current_screen_border=colors[9], active=colors[10], inactive=colors[5], background=colors[12])
widget_windowname = widget.WindowName(foreground=colors[11],fmt='{}')
widget_chord = widget.Chord(chords_colors={'launch': ("#ff0000", "#ffffff"),}, name_transform=lambda name: name.upper(),)
widget_update = widget.CheckUpdates(
                    update_interval=10,
                    distro="Arch_checkupdates",
                    display_format='⟲ {updates}',
                    foreground=colors[11],
                    #mouse_callbacks={
                    #    'Button1':
                    #    lambda: qtile.cmd_spawn('xfce4-terminal -e "yay -Syu --noconfirm"')
                    #},
                    background=colors[3])
widget_clock = widget.Clock(format=' %d.%m.%Y %H:%M',
                             background=colors[1],
                             foreground=colors[6])
widget_power = widget.TextBox(
                    text='⏻ ',
                    mouse_callbacks= {
                        'Button1':
                        lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
                    },
                    foreground=colors[6],
                    background=colors[0]
                )
widget_battery = widget.Battery(
                        format='{char}{percent:2.0%}',
                        charge_char='🗲',
                        discharge_char='',
                        full_char='',
                        background=colors[3],
                        foreground=colors[11]
                        )



#######################
#      FUNCTIONS      #
#######################


def get_screen_main():
    return Screen(
        top=bar.Bar(
            [   widget_sep1,
                widget_pic,
                widget_sep2,
                widget_groups,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[12]),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget_windowname,
                widget_chord,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[3], background=colors[4]),
                #widget_update,
                widget_battery,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[2], background=colors[3]),
                widget.Systray(icon_size = 20, background=colors[2]),
                volume,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[1], background=colors[2]),
                widget_clock,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[0], background=colors[1]),
                widget.CurrentLayoutIcon(scale=0.75, background=colors[0]),
                widget_power
            ],
            bar_height,  # height in px
            background=colors[4],  # background color
            margin=bar_margin
        ),
        #y=bar_y 
    )

def get_screen_side1():
    return Screen(
        top=bar.Bar(
            [   widget_sep1,
                widget_pic,
                widget_sep2, 
                widget_groups,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[3], background=colors[4]),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget_windowname,
                widget_chord,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[3], background=colors[4]), 
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[2], background=colors[3]), 
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[1], background=colors[2]),    
                widget_clock,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[0], background=colors[1]),
                widget.CurrentLayoutIcon(scale=0.75, background=colors[0]),
                widget_power
            ],
            bar_height,  # height in px
            background=colors[4],  # background color
            margin=bar_margin
        ), )



def get_screen_side2():
    return Screen(
        top=bar.Bar(
            [   widget_sep1,
                widget_pic,
                widget_sep2, 
                widget_groups,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[3], background=colors[4]),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget_windowname,
                widget_chord,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[3], background=colors[4]), 
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[2], background=colors[3]), 
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[1], background=colors[2]),    
                widget_clock,
                widget.TextBox(text = '', padding = 0, fontsize = 28, foreground=colors[0], background=colors[1]),
                widget.CurrentLayoutIcon(scale=0.75, background=colors[0]),
            ],
            bar_height,  # height in px
            background=colors[4],  # background color
            margin=bar_margin
        ), )



screens = [get_screen_main(), get_screen_side1(), get_screen_side2()]
