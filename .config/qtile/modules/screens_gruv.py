from libqtile import bar
from .widgets import *
from libqtile.config import Screen

#from qtile_extra.bar import Bar
#from qtile_extra.widget import modify

import os

bar_height = 23
bar_margin = [5,10,0,10]

padding_sides = 6


#####################
#       COLOR       #
#####################


colors = [["#00000000", "#00000000"], # -0- background transparent
          ["#7c6f64", "#7c6f64"], # -1- background panel 1
          ["#1EA023", "#1EA023"], # -2- free
          ["#4BCB50", "#4BCB50"], # -3- free
          ["#93E496", "#93E496"], # -4- free
          ["#83a598", "#83a598"], # -5- font color inactive
          ["#fbf1c7", "#fbf1c7"], # -6- font color
          ["#1ea023", "#1ea023"], # -7- free
          ["#665c54", "#665c54"], # -8- backbround for current screens
          ["#d3869b", "#d3869b"], # -9- underline current screen
          ["#fabd2f", "#fabd2f"], # -10- active font
          ["#3E3E3C", "#3E3E3C"], # -11- font color middle
          ["#4BCB50", "#4BCB50"]] # -12- free





#####################
#      WIDGETS      #
#####################

widget_sep1 = widget.Sep(padding=padding_sides, linewidth=0, background=colors[12])
widget_sep2 = widget.Sep(padding=4, linewidth=0, background=colors[12])
widget_groups = widget.GroupBox(highlight_method='line', highlight_color=colors[8], this_screen_border=colors[9], this_current_screen_border=colors[9], active=colors[10], inactive=colors[5], background=colors[1])
widget_windowname = widget.WindowName(foreground=colors[6], background=colors[1], fmt="{}")
widget_chord = widget.Chord(chords_colors={'launch': ("#ff0000", "#ffffff"),}, name_transform=lambda name: name.upper(),)
widget_update = widget.CheckUpdates(
                    update_interval=180,
                    distro='Arch_checkupdates',
                    display_format='⟲ {updates}',
                    no_update_string='✓' ,
                    foreground=colors[6],
                    mouse_callbacks={
                        'Button1':
                        lambda: qtile.cmd_spawn('xfce4-terminal -e "yay -Syu --noconfirm"')
                    },
                    background=colors[1])
widget_clock = widget.Clock(format='%H:%M',
                             background=colors[1],
                             foreground=colors[6])
widget_date = widget.Clock(format='%d.%m.%Y',
                             background=colors[1],
                             foreground=colors[6])
widget_power = widget.TextBox(
                    text='⏻ ',
                    mouse_callbacks= {
                        'Button1':
                        lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
                    },
                    foreground=colors[6],
                    background=colors[0]
                )
widget_battery = widget.Battery(
                        format='{char} {percent:2.0%}',
                        charge_char='🗲',
                        discharge_char='',
                        full_char='',
                        update_interval=3,
                        background=colors[1],
                        foreground=colors[6]
                        )
widget_cpuIcon = widget.TextBox(text='', fontsize=23, foreground=colors[6], background=colors[1])
widget_gpuIcon = widget.TextBox(text='﬙', fontsize=23, foreground=colors[6], background=colors[1])
widget_ramIcon = widget.TextBox(text='', fontsize=23, foreground=colors[6], background=colors[1])
widget_batIcon = widget.TextBox(text='', fontsize=12, foreground=colors[6], background=colors[1])
widget_cpuTempCthulhu = widget.ThermalSensor(threshold=90, update_interval=5, tag_sensor='CPUTIN', foreground=colors[6], background=colors[1])
widget_cpuTempNzoth = widget.ThermalSensor(threshold=90, update_interval=5, tag_sensor='Package id 0', foreground=colors[6], background=colors[1])
widget_gpuTemp = widget.NvidiaSensors(format='{temp}°C', update_interval=3, foreground=colors[6], background=colors[1])
widget_cpuPer = widget.CPU(format='{load_percent}%', update_interval=3, foreground=colors[6], background=colors[1])
widget_ramPer = widget.Memory(format='{MemUsed:.1f}{mm}', measure_mem='G', update_interval=3, foreground=colors[6], background=colors[1])
widget_sep3 = widget.Spacer(length=7)
widget_emptyText = widget.TextBox(text='  ', fontsize=10, foreground=colors[6], background=colors[1])


#######################
#      FUNCTIONS      #
#######################


def get_screen_main():
    main_bar = [
            widget.Spacer(length=7),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_date,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_cpuIcon,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.CurrentLayoutIcon(scale=0.75, background=colors[1]),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.Spacer(size_percent=30),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.GroupBox(highlight_method='line', highlight_color=colors[8], this_screen_border=colors[9], this_current_screen_border=colors[9], active=colors[10], inactive=colors[5], background=colors[1]),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            #widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            #widget_windowname,
            #widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.Spacer(size_percent=30),
            widget.Systray(icon_size = 20, background=colors[0]),
            volume,
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_update,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_cpuIcon,
            widget_cpuPer,
            widget_emptyText,
            widget_ramIcon,
            widget_ramPer,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_clock,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
    ]

    if os.path.exists("/dev/nvidia0"):
        main_bar.insert(7, widget_cpuTempCthulhu)
        main_bar.insert(8, widget_gpuTemp)
        main_bar.insert(8, widget_gpuIcon)
        main_bar.insert(8, widget_emptyText)
    
    if len(os.listdir("/sys/class/power_supply"))!=0:
        main_bar.insert(7, widget_cpuTempNzoth)
        main_bar.insert(-3, widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]))
        main_bar.insert(-3, widget_battery)
        main_bar.insert(-3, widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]))
        main_bar.insert(-3, widget_sep3)
    

    return Screen(
        top=bar.Bar(
            main_bar,
            bar_height,  # height in px
            background=colors[0],  # background color
            margin=bar_margin
        ),
    )

def get_screen_side():
    main_bar = [
            widget.Spacer(length=7),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_date,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_cpuIcon,
            widget_cpuTempCthulhu,
            widget_emptyText,
            widget_gpuIcon,
            widget_gpuTemp,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.CurrentLayoutIcon(scale=0.75, foreground=colors[6], background=colors[1]),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.Spacer(size_percent=30),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.GroupBox(highlight_method='line', highlight_color=colors[8], this_screen_border=colors[9], this_current_screen_border=colors[9], active=colors[10], inactive=colors[5], background=colors[1]),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            #widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            #widget_windowname,
            #widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget.Spacer(size_percent=30),
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_cpuIcon,
            widget_cpuPer,
            widget_emptyText,
            widget_ramIcon,
            widget_ramPer,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_sep3,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
            widget_clock,
            widget.TextBox(text = '', padding = 0, fontsize = 22, foreground=colors[1]),
    ]

    return Screen(
        top=bar.Bar(
            main_bar,
            bar_height,  # height in px
            background=colors[0],  # background color
            margin=bar_margin
        ),
    )


screens = [get_screen_main(), get_screen_side(), get_screen_side()]
