from typing import List  # noqa: F401

from libqtile import qtile
from libqtile import layout, bar, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Rule
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

import os
import re
import socket
import subprocess

mod = "mod4"
terminal = "xfce4-terminal"
keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    #Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    Key([mod, "shift"], "d", lazy.spawn("dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'"), desc="Launch dmenu"),

    # custom dmenu scripts
    Key([mod, "shift"], "c", lazy.spawn("/home/olli/dmscripts/dmconf | dmenu_run"), desc="Launch dmenu"),

    # shortcuts
    Key([mod], "x", lazy.spawn("arcolinux-logout"), desc="Logout"),

]

group_names = [("WWW", {'layout': 'Columns'}),
               ("DEV", {'layout': 'Columns'}),
               ("SYS", {'layout': 'Columns'}),
               ("DOC", {'layout': 'Columns'}),
               ("GAME", {'layout': 'Columns'}),
               ("CHAT", {'layout': 'Columns'}),
               ("MUS", {'layout': 'Columns'}),
               ("VID", {'layout': 'Columns'}),
               ("GFX", {'layout': 'Columns'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 3,
                "margin": 8,
                "border_focus": "25859B",
                "border_normal": "1D2330"
                }

layouts = [
    layout.Columns(**layout_theme, border_on_single = True, border_focus_stack='25859B', border_normal_stack='1D2330'),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    layout.Stack(num_stacks=2),
    layout.Bsp(),
    layout.Matrix(),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(),
    layout.RatioTile(),
    layout.Tile(),
    layout.TreeTab(),
    layout.VerticalTile(),
    layout.Zoomy(),
]

widget_defaults = dict(
    font='Ubuntu Mono',
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()

colors = [["#282c34", "#282c34"], # -0- panel background
          ["#3d3f4b", "#434758"], # -1- background for current screen tab
          ["#ffffff", "#ffffff"], # -2- font color for group names
          ["#857915", "#857915"], # -3- border line color for current tab
          ["#1D5F59", "#1D5F59"], # -4- border line color for 'other tabs' and color for 'odd widgets'
          ["#857915", "#857915"], # -5- color for the 'even widgets'
          ["#25859B", "#25859B"], # -6- window name
          ["#ecbbfb", "#ecbbfb"], # -7- backbround for inactive screens
          ["#B89433", "#B89433"]] # -8- font color for other group names


def init_widgets_list():
    widgets_list = [
            widget.Sep(
                   linewidth = 0,
                   padding = 6,
                   foreground = colors[2],
                   background = colors[0]
                   ),
            widget.Image(
                   filename = "~/.config/qtile/icons/python-white.png",
                   scale = "False",
                   mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal)}
                   ),
            widget.Sep(
                   linewidth = 0,
                   padding = 6,
                   foreground = colors[2],
                   background = colors[0]
                   ),
            widget.GroupBox(
                   font = "Ubuntu Bold",
                   fontsize = 9,
                   margin_y = 3,
                   margin_x = 0,
                   padding_y = 5,
                   padding_x = 3,
                   borderwidth = 3,
                   active = colors[2],
                   inactive = colors[8],
                   rounded = False,
                   highlight_color = colors[1],
                   highlight_method = "line",
                   this_current_screen_border = colors[3],
                   this_screen_border = colors [4],
                   other_current_screen_border = colors[3],
                   other_screen_border = colors[4],
                   foreground = colors[2],
                   background = colors[0]
                   ),
            widget.Sep(
                   linewidth = 0,
                   padding = 40,
                   foreground = colors[2],
                   background = colors[0]
                   ),
            widget.WindowName(
                   foreground = colors[6],
                   background = colors[0],
                   padding = 0
                   ),
            widget.Systray(
                   background = colors[3],
                   padding = 5
                   ),
            widget.Sep(
                   linewidth = 0,
                   padding = 6,
                   foreground = colors[2],
                   background = colors[0]
                   ),
            widget.TextBox(
                   text = '',
                   background = colors[0],
                   foreground = colors[5],
                   padding = 0,
                   fontsize = 37
                   ),
            widget.TextBox(
                   text = 'CPU:',
                   background = colors[5],
                   foreground = colors[2],
                   padding = 0,
                   fontsize = 14
                   ),
            widget.ThermalSensor(
                   foreground = colors[2],
                   background = colors[5],
                   threshold = 90,
                   padding = 5,
                   update_interval = 1,
                   tag_sensor = "Tdie",
                   ),
            widget.TextBox(
                   text = '',
                   background = colors[5],
                   foreground = colors[4],
                   padding = 0,
                   fontsize = 37
                   ),
            widget.Memory(
                   foreground = colors[2],
                   background = colors[4],
                   mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e htop')},
                   padding = 5
                   ),
            widget.TextBox(
                   text = '',
                   background = colors[4],
                   foreground = colors[5],
                   padding = 0,
                   fontsize = 37
                   ),
            widget.TextBox(
                   text = " ⟳",
                   padding = 2,
                   foreground = colors[2],
                   background = colors[5],
                   fontsize = 14
                   ),
            widget.CheckUpdates(
                   update_interval = 1800,
                   distro = "Arch_checkupdates",
                   display_format = "{updates} Updates",
                   foreground = colors[2],
                   mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syu')},
                   background = colors[5]
                   ),
            widget.TextBox(
                   text = '',
                   background = colors[5],
                   foreground = colors[4],
                   padding = 0,
                   fontsize = 37
                   ),
            widget.CurrentLayoutIcon(
                   #custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                   foreground = colors[0],
                   background = colors[4],
                   padding = 0,
                   scale = 0.7
                   ),
            widget.CurrentLayout(
                   foreground = colors[2],
                   background = colors[4],
                   padding = 5
                   ),
            widget.TextBox(
                   text = '',
                   background = colors[4],
                   foreground = colors[5],
                   padding = 0,
                   fontsize = 37
                   ),
            widget.Clock(
                   foreground = colors[2],
                   background = colors[5],
                   format = "%d.%B - %H:%M "
                   )
    ]
    return widgets_list


def get_screen1_widgets():
    widget_screen2 = init_widgets_list()
    del widget_screen2[6:7]
    return widget_screen2



def init_screens():
    return [Screen(top=bar.Bar(widgets=get_screen1_widgets(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=get_screen1_widgets(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_list(), opacity=1.0, size=20))]

screens = init_screens()


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'Arcolinux-welcome-app.py'},
    {'wmclass': 'Arcolinux-tweak-tool.py'},
    {'wmclass': 'Arcolinux-calamares-tool.py'},
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},
    {'wmclass': 'makebranch'},
    {'wmclass': 'maketag'},
    {'wmclass': 'Arandr'},
    {'wmclass': 'feh'},
    {'wmclass': 'Galculator'},
    {'wmclass': 'arcolinux-logout'},
    {'wmclass': 'xfce4-terminal'},
    {'wname': 'branchdialog'},
    {'wname': 'Open File'},
    {'wname': 'pinentry'},
    {'wmclass': 'ssh-askpass'},

],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True
focus_on_window_activation = "smart"


#@hook.subscribe.startup_once
#def start_once():
#    home = os.path.expanduser('~')
#    subprocess.call([home + '/.config/qtile/autostart.sh'])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"